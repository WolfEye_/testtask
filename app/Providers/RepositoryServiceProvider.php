<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\PeopleRepository::class, \App\Repositories\PeopleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FilmRepository::class, \App\Repositories\FilmRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\HomeWorldRepository::class, \App\Repositories\HomeWorldRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\GenderRepository::class, \App\Repositories\GenderRepositoryEloquent::class);
        //:end-bindings:
    }
}
