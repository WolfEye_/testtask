<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeopleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:100',
            'height' => 'required|integer',
            'mass' => 'required|integer',
            'hair_color' => 'required|min:2|max:10',
            'birth_year' => 'required|',
            'gender_id' => 'required|integer',
            'homeworld_id' => 'required|integer',
            'url' => 'required|'
        ];
    }
}
