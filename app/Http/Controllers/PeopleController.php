<?php

namespace App\Http\Controllers;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PeopleCreateRequest;
use App\Http\Requests\PeopleUpdateRequest;
use App\Repositories\PeopleRepository;
use App\Repositories\FilmRepository;
use App\Repositories\HomeWorldRepository;
use App\Repositories\GenderRepository;
use App\Validators\PeopleValidator;

/**
 * Class PeopleController.
 *
 * @package namespace App\Http\Controllers;
 */
class PeopleController extends Controller
{
    /**
     * @var PeopleRepository
     */
    protected $repository;

    /**
     * @var PeopleValidator
     */
    protected $validator;

    /**
     * PeopleController constructor.
     *
     * @param PeopleRepository $repository
     * @param PeopleValidator $validator
     */
    public function __construct(PeopleRepository $repository, PeopleValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $peoples = $this->repository->with(['films', 'homeworld'])->paginate(10);

        return view('people.index', compact('peoples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param GenderRepository $genderRepository
     * @param HomeWorldRepository $homeWorldRepository
     * @param FilmRepository $filmRepository
     * @return Response
     */
    public function create(GenderRepository $genderRepository, HomeWorldRepository $homeWorldRepository,
                           FilmRepository $filmRepository)
    {
        $fields = [
            'name', 'height', 'mass', 'hair_color', 'birth_year', 'url'
        ];

        $genders = $genderRepository->all();
        $homeWorlds = $homeWorldRepository->all();
        $films = $filmRepository->all();

        return view('people.create', compact('genders', 'homeWorlds', 'films', 'fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PeopleCreateRequest $request
     *
     * @param FilmRepository $filmRepository
     * @return \Illuminate\Http\Response
     *
     */
    public function store(PeopleCreateRequest $request, FilmRepository $filmRepository)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $people = $this->repository->create($request->all());

            $films = $filmRepository->findWhereIn('id', $request->input('films_id'));
            $people->films()->sync($films);

            $response = [
                'message' => 'People created.'
            ];

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @param HomeWorldRepository $homeWorldRepository
     * @return \Illuminate\Http\Response
     */
    public function show($id, HomeWorldRepository $homeWorldRepository)
    {
        $homeWorlds = $homeWorldRepository->with(['peoples'])->paginate(10);

        return view('people.show', compact('homeWorlds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @param GenderRepository $genderRepository
     * @param HomeWorldRepository $homeWorldRepository
     * @param FilmRepository $filmRepository
     * @return \Illuminate\Http\Response
     */
    public function edit($id, GenderRepository $genderRepository, HomeWorldRepository $homeWorldRepository,
                         FilmRepository $filmRepository)
    {
        $fields = [
            'id', 'name', 'height', 'mass', 'hair_color', 'birth_year', 'url'
        ];

        $people = $this->repository->find($id);
        $genders = $genderRepository->all();
        $homeWorlds = $homeWorldRepository->all();
        $films = $filmRepository->all();

        return view('people.edit', compact('people', 'homeWorlds', 'genders', 'films', 'fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PeopleUpdateRequest $request
     * @param string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PeopleUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'People updated.'
            ];

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return redirect()->back()->with('message', 'People deleted.');
    }
}
