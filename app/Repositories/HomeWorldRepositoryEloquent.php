<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HomeWorldRepository;
use App\Models\HomeWorld;
use App\Validators\HomeWorldValidator;

/**
 * Class HomeWorldRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class HomeWorldRepositoryEloquent extends BaseRepository implements HomeWorldRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HomeWorld::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
