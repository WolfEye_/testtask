<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HomeWorldRepository.
 *
 * @package namespace App\Repositories;
 */
interface HomeWorldRepository extends RepositoryInterface
{
    //
}
