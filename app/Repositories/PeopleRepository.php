<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PeopleRepository.
 *
 * @package namespace App\Repositories;
 */
interface PeopleRepository extends RepositoryInterface
{
    //
}
