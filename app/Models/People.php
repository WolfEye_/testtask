<?php

namespace App\Models;

use App\Models\Film;
use App\Models\HomeWorld;
use App\Models\Gender;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class People extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'people';

    protected $fillable = [
        'name','height','mass','hair_color','birth_year','gender_id','homeworld_id', 'url'
    ];

    public function films(){
        return $this->belongsToMany(Film::class, 'film_people');
    }

    public function homeworld(){
        return $this->belongsTo(HomeWorld::class);
    }

    public function gender(){
        return $this->belongsTo(Gender::class);
    }
}
