<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class HomeWorld extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'homeworld';

    public function peoples(){
        return $this->hasMany(People::class,'homeworld_id');
    }
}
