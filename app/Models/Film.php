<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Film extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'films';

    protected $fillable = [
        'title', 'url'
    ];
}
