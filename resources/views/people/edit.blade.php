@extends('layouts.app')

@section('content')
    <div class="row col-12">
        <form id="form" class="col-lg-5 mx-auto" action="{{ route('peoples.update', $people) }}" method="POST">
            @csrf
            @method('PATCH')

            @if (isset($errors) ? $errors->any() : false)
                <div class="col-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if (\Session::has('message'))
                <div class="col-12">
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ \Session::get('message')}}</li>
                        </ul>
                    </div>
                </div>
            @endif
            @foreach($fields as $field)
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">{{$field}}:</label>
                    <div class="col-sm-9">
                        <input type="text"
                               id="{{$field}}"
                               name="{{$field}}"
                               class="form-control"
                               placeholder="Enter {{$field}}..."
                               value="{{$people->$field}}"
                               required>
                    </div>
                </div>
            @endforeach
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">gender:</label>
                <div class="col-sm-9">
                    <select id="gender_id" name="gender_id" class="form-control">
                        <option value="" disabled>Choose your option</option>
                        {{$i = 1}}
                        @foreach($genders as $gender)
                            <option
                                {{ $gender['name'] === $people->gender_id['name'] ? 'selected="selected"' : '' }}
                                value="{{$i++}}">{{$gender['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">home world:</label>
                <div class="col-sm-9">
                    <select id="homeworld_id" name="homeworld_id" class="form-control">
                        <option value="" disabled>Choose your option</option>
                        {{$i = 1}}
                        @foreach($homeWorlds as $homeWorld)
                            <option
                                {{ $homeWorld['name'] === $people->homeworld['name'] ? 'selected="selected"' : '' }}
                                value="{{$i++}}">{{$homeWorld['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">films:</label>
                <div class="col-sm-9">
                    <select id="films_id" name="films_id[]" class="form-control" multiple="multiple">
                        {{$i = 1}}
                        @foreach($films as $film)
                            <option
                                @foreach($people->films as $peopleFilm)
                                {{ $film['title'] === $peopleFilm['title'] ? 'selected="selected"' : '' }}
                                @endforeach
                                value="{{$i++}}">{{$film['title']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <a class="btn btn-primary btn-lg"
                   id="sendMessage"
                   href="{{ route('peoples.update', $people) }}">Edit</a>
            </div>
        </form>
    </div>


    <script>
        $(document).ready(() => {
            const inputs = [
                $('#height'),
                $('#mass'),
                $('#gender_id'),
                $('#homeworld_id')];

            const url = $('#url').val();
            const errors = [];

            $('#sendMessage').on('click', (e) => {
                e.preventDefault();

                let success = true;

                const selected = $('#films_id option:checked');
                const values = Array.from(selected).map(
                    el => Number.isInteger(+el.value) ? +el.value :
                        success ? errors.push('films_id') :
                            errors.length > 0 ? success = false : ''
                );

                inputs.forEach(function (item) {
                    Number.isInteger(+item.val()) ? '' : success = false;
                    Number.isInteger(+item.val()) ? '' : errors.push(item.attr('id'));
                });

                const regex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/i;
                regex.test(url)

                if (success) {
                    $('#form').submit();
                } else {
                    errors.forEach(function (item) {
                        $(`#${item}`).addClass('alert alert-danger')
                    });
                }
            })
        });
    </script>
@endsection
