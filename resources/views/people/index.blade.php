@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Height</th>
                    <th>Mass</th>
                    <th>Hair Color</th>
                    <th>Birth Year</th>
                    <th>Gender</th>
                    <th>HomeWorld</th>
                    <th>Films</th>
                    <th>Url</th>
                    <th>Created_at</th>
                    <th>Updated_at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if ($peoples->total() === 0)
                    <tr>
                        <td colspan="3" class="text-center">Empty...</td>
                    </tr>
                @else
                    @foreach($peoples->items() as $item)
                        <tr>
                            <th>{{ $item->id }}</th>
                            <th>{{ $item->name }}</th>
                            <th>{{ $item->height }}</th>
                            <th>{{ $item->mass }}</th>
                            <th>{{ $item->hair_color }}</th>
                            <th>{{ $item->birth_year }}</th>
                            <th>{{ $item->gender['name'] }}</th>
                            <th>
                                <a href="{{ $item->homeworld['url'] }}">{{ $item->homeworld['name'] }}</a>
                            </th>
                            <th>
                                <div class="dropdown show">
                                    <a class="btn btn-primary dropdown-toggle" href="#" role="button"
                                       id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">Films</a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        @foreach($item->films as $film)
                                            <a class="dropdown-item" href="{{ ($film['url']) }}">{{$film['title']}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </th>
                            <th>
                                <a href="{{ $item->url }}">{{ $item->url }}</a>
                            </th>
                            <th>{{ $item->created_at }}</th>
                            <th>{{ $item->updated_at }}</th>
                            <td class="text-right">
                                <div class="btn-group text-left">
                                    <a href="{{ route('peoples.edit', $item->id) }}"
                                       class="btn btn-primary btn-sm" data-toggle="tooltip"
                                       data-placement="top" title="Редактировать"><i class="fas fa-edit"></i></a>
                                    <form id="delete-form-{{ $item->id }}" action="{{ route('peoples.destroy', $item->id) }}" method="POST" style="display: none;">
                                        @csrf
                                        @method('delete')
                                    </form>

                                    <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#questionModal-{{ $item->id }}"><i class="fas fa-trash"></i></a>

                                    <div class="modal fade" id="questionModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Вы уверены что хотите удалить?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-right">
                                                    <a class="btn btn-sm btn-danger" href="{{ route('peoples.destroy', $item->id) }}"
                                                       onclick="event.preventDefault();
                                                           document.getElementById('delete-form-{{ $item->id }}').submit();"><i class="fas fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="col-12">
            {{$peoples->links()}}
        </div>
    </div>
@endsection
