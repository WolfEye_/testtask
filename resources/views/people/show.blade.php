@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Planet Name</th>
                    <th class="row" style="justify-content: space-between; margin: 0">
                        <span>Peoples</span>
                        <span>Films</span>
                    </th>
                </tr>
                </thead>
                <tbody>
                @if ($homeWorlds->total() === 0)
                    <tr>
                        <td colspan="3" class="text-center">Empty...</td>
                    </tr>
                @else
                    @foreach($homeWorlds->items() as $item)
                        <tr>
                            <th>{{ $item->id }}</th>
                            <th>
                                <a href="{{ $item->url }}">{{ $item->name }}</a>
                            </th>
                            <th>
                                <ul style="padding-left: 15px;">
                                    @foreach($item->peoples as $people)
                                        <li class="row" style="justify-content: space-between">
                                            <a href="{{ $people->url }}">{{ $people->name }}</a>

                                            <div class="dropdown show">
                                                <a class="dropdown-toggle text-right"
                                                   href="#" role="button"
                                                   id="dropdownMenuLink"
                                                   data-toggle="dropdown"
                                                   aria-haspopup="true"
                                                   aria-expanded="false">Films</a>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    @foreach($people->films as $film)
                                                        <a class="dropdown-item"
                                                           href="{{ ($film['url']) }}">{{$film['title']}}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="col-12">
            {{$homeWorlds->links()}}
        </div>
    </div>
@endsection
