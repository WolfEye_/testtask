<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <a class="navbar-brand" href="{{route('peoples.index')}}">Home</a>
            <div class="collapse navbar-collapse" id="navbarsExample04">
                <ul class="pl-4 navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('peoples.create')}}">Create<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('peoples.show', 0)}}">Planets<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
