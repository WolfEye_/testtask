<?php

use Illuminate\Database\Seeder;
use App\Models\People;
use App\Models\HomeWorld;
use App\Models\Film;
use App\Models\Gender;

class DatabaseTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //------ adding Planets to the database ------------

        $pagesCounter = 1;

        do {
            $homeWorldsInfo = json_decode(file_get_contents('https://swapi.co/api/planets/?page=' . ($pagesCounter++)), true);
            foreach ($homeWorldsInfo['results'] as $homeWorld) {
                HomeWorld::create([
                    'name' => $homeWorld['name'],
                    'url' => $homeWorld['url']
                ]);
            }
        } while ($homeWorldsInfo['next'] != null);


        //------ adding Films to the database ------------

        $pagesCounter = 1;

        do {
            $filmsInfo = json_decode(file_get_contents('https://swapi.co/api/films/?page=' . ($pagesCounter++)), true);
            foreach ($filmsInfo['results'] as $film) {
                Film::create([
                    'title' => $film['title'],
                    'url' => $film['url']
                ]);
            }
        } while ($filmsInfo['next'] != null);



        //------ adding Peoples and Genders to the database ------------

        $pagesCounter = 1;

        do {
            $peopleInfo = json_decode(file_get_contents('https://swapi.co/api/people/?page=' . ($pagesCounter++)), true);

            foreach ($peopleInfo['results'] as $people) {
                $peopleModel = People::create([
                    'name' => $people['name'],
                    'height' => is_numeric($people['height']) ? floatval($people['height']) : null,
                    'mass' => is_numeric($people['mass']) ? floatval($people['mass']) : null,
                    'hair_color' => $people['hair_color'],
                    'birth_year' => $people['birth_year'],
                    'gender_id' => Gender::firstOrCreate(['name' => $people['gender']])->id,
                    'homeworld_id' => HomeWorld::where('url', $people['homeworld'])->first()->id,
                    'url' => $people['url'],
                ]);

                $films = Film::whereIn('url', $people['films'])->get();

                $peopleModel->films()->sync($films);
            }
        } while ($peopleInfo['next'] != null);
    }
}
