<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('height')->nullable();
            $table->integer('mass')->nullable();
            $table->string('hair_color');
            $table->string('birth_year');
            $table->unsignedInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('gender');
            $table->unsignedInteger('homeworld_id');
            $table->foreign('homeworld_id')->references('id')->on('homeworld');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
